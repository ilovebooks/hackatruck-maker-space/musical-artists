//
//  ArtistDescriptionViewController.swift
//  ArtistasMusicais
//
//  Created by Student on 16/09/22.
//  Copyright © 2022 Student. All rights reserved.
//

import UIKit

class ArtistDescriptionViewController: UIViewController {
    var artist: Artist?
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        artistImage.image = UIImage(named: artist!.artistPhoto)
        artistNameLabel.text = artist!.artistName
        descriptionTextView.text = artist?.artistDescription
    }
}
