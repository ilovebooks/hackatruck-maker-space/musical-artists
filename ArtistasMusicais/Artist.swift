
import Foundation

class Artist {
    let artistName: String
    let artistPhoto: String
    let artistDescription : String
    
    init(_ artistName: String, _ artistPhoto: String, _ artistDescription: String) {
        self.artistName = artistName
        self.artistPhoto = artistPhoto
        self.artistDescription = artistDescription
    }
}

class ArtistDAO {
    static func getItem() -> Artist {
        return
            Artist(
                "Dream Theater",
                "dreamtheater",
                "Dream Theater is an American progressive metal band formed in 1985 under the name Majesty by John Petrucci, John Myung and Mike Portnoy while they attended Berklee College of Music in Boston, Massachusetts. They subsequently dropped out of their studies to concentrate further on the band that would eventually become Dream Theater. Their current lineup consists of Petrucci, Myung, vocalist James LaBrie, keyboardist Jordan Rudess and drummer Mike Mangini. Over the course of various lineup changes, Petrucci and Myung have been the only two constant members, and Portnoy remained with the band until 2010, when he left to pursue other musical endeavors and he has since been replaced by Mangini. After a brief stint with Chris Collins, followed by Charlie Dominici (who was dismissed from Dream Theater not long after the release of their first album), LaBrie was hired as the band's singer in 1991. Dream Theater's first keyboardist, Kevin Moore, left the band after three albums and was replaced by Derek Sherinian in 1995 after a period of touring. After just one album with Sherinian, they replaced him with current keyboardist Jordan Rudess in 1999."
            )
    }
}
