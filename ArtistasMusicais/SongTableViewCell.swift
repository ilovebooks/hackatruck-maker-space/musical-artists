//
//  SongTableViewCell.swift
//  ArtistasMusicais
//
//  Created by Student on 19/09/22.
//  Copyright © 2022 Student. All rights reserved.
//

import UIKit

class SongTableViewCell: UITableViewCell {
    @IBOutlet weak var albumImageVIew: UIImageView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
}
