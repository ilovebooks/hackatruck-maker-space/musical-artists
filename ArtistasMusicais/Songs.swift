import Foundation

class Song {
    let name: String
    let artist: String
    let album: String
    let albumImage: String
    
    init(_ name: String, _ artist: String, _ album: String, _ albumImage: String) {
        self.name = name
        self.artist = artist
        self.album = album
        self.albumImage = albumImage
    }
}

class SongDAO {
    static func getList() -> [Song] {
        return [
            Song("The Spirit Carries On", "Dream Theater", "Metropolis Pt. 2: Scenes from a Memory", "metropolispt2"),
            Song("Unleashed", "Epica", "Design Your Universe", "design-your-universe"),
            Song("The Count Of Tuscany", "Dream Theater", "Black Clouds & Silver Linings", "black-clouds-silver-linings"),
            Song("Civilizaiton in the Sky", "Rainbowdragoneyes", "The Messenger Disc II: The Future", "the-messenger"),
            Song("Wedding Nails", "Porcupine Tree", "In Absentia", "in-absentia"),
            Song("Amaranth", "Nightwish", "Dark Passion Play", "dark-passion-play"),
            Song("Octavarium", "Dream Theater", "Octavarium", "octavarium"),
            Song("Romantic Children", "ZUN", "Touhou 5: Mystic Square", "touhou")
        ]
    }
}
