//
//  ArtistViewController.swift
//  ArtistasMusicais
//
//  Created by Student on 16/09/22.
//  Copyright © 2022 Student. All rights reserved.
//

import UIKit

class ArtistViewController : UIViewController {
    var artist: Artist?
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        artist = ArtistDAO.getItem()
        artistImage.image = UIImage(named: artist!.artistPhoto)
        artistNameLabel.text = artist!.artistName
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Artist" {
            if let newView = segue.destination as? ArtistDescriptionViewController {
                newView.artist = artist
            }
        }
    }
}
