//
//  SongViewController.swift
//  ArtistasMusicais
//
//  Created by Student on 19/09/22.
//  Copyright © 2022 Student. All rights reserved.
//

import UIKit

class SongViewController: UIViewController {
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    
    var song: Song?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        albumImageView.image = UIImage(named: song!.albumImage)
        songNameLabel.text = song!.name
        albumNameLabel.text = song!.album
        artistNameLabel.text = song!.artist
    }
}
